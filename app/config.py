import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    GRAPH_URL: str
    WS_URL: str
    DB_URL: str
    ABI_PATH: str
    DIAMOND_ADDRESS_POLYGON: str
    WEB3_HTTP_PROVIDER_POLYGON: str

    class Config:
        env_file = f'envs/.env.{os.environ.get("APP_ENV", "dev")}'
        env_file_encoding = "utf-8"
        case_insensitive = True


settings = Settings()
