from enum import IntEnum
from typing import Optional, Any
from eth_typing.evm import HexAddress
from pydantic import BaseModel, Field, validator
from web3 import Web3


class ChainId(IntEnum):
    ETH = 1
    MATIC = 137
    BNB = 56
    CELO = 42220


class Account(BaseModel):
    address: HexAddress
    chainID: int
    isActive: bool
    lastCalculatedBlock: int


class Pool(BaseModel):
    id: str
    accounts: list[Account]


class Deposit(BaseModel):
    pool: Pool


class GraphResponse(BaseModel):
    deposits: list[Deposit]


class DBRequest(BaseModel):
    chain_id: ChainId
    addresses: list[HexAddress]
    block_start: int = Field(default=0)
    block_end: int = Field(default=999999999)


class DBResult(BaseModel):
    chain_id: ChainId
    address: HexAddress
    block_start: int
    block_end: int
    timestamp_start: int
    timestamp_end: int
    gas_used: int
    tx_fee: int
    co2: float
    tx_count: int


class WsRequest(BaseModel):
    chain_id: ChainId
    addresses: list[HexAddress]
    block_start: int = Field(default=0)
    block_end: int = Field(default=0)


class WsResult(BaseModel):
    chain_id: ChainId
    address: HexAddress
    block_start: int
    block_end: int
    timestamp_start: int
    timestamp_end: int
    gas_used: int
    tx_fee: int
    co2: float
    tx_count: int


class WsResponse(BaseModel):
    uid: HexAddress | None
    status: str = "1"
    message: str = "OK"
    client_id: str
    result: Optional[WsResult] = None


class AccountUpdate(BaseModel):
    chainId: int
    address_: str
    knownLastCalculatedBlock: int
    newLastCalculatedBlock: int

    @validator('address_')
    def checksum(cls, v):
        return Web3.to_checksum_address(v)


class FacetSubmitData(BaseModel):
    coolPoolID: int
    updates:Optional[list[AccountUpdate]] = []
    targetAddressesTxCounts: Optional[list[int]] = []
    targetAddressesEmissions: Optional[list[int]] = []
