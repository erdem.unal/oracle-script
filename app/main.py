import asyncio
import json

import aiohttp
import websockets
from config import settings
from pydantic import ValidationError
from schemas import (
    FacetSubmitData,
    GraphResponse,
    WsRequest,
    WsResponse,
    AccountUpdate,
    ChainId,
    DBRequest,
    DBResult,
    Account
)
from web3 import Web3
from web3.exceptions import ContractLogicError
from typing import Iterable
from web3.middleware import geth_poa_middleware


class OffsetOracle:

    """
    A class that represents an offset oracle for CO2 emissions. It provides methods for retrieving data about CO2 emissions from different pools and submitting that data to a smart contract on the Polygon network.

    Attributes:
        None

    Methods:
        get_coolpool_details(): retrieves information about cool pools and their associated accounts from a GraphQL endpoint.
        get_emission_data(db_request: DBRequest) -> list[DBResult]: retrieves emission data from a database using a given DBRequest.
        connect_to_websocket(message: WsRequest): establishes a WebSocket connection and sends a message.
        process_accounts(ws_request: WsRequest, account: Account, facet_data: FacetSubmitData) -> bool: processes emission data for a given account and adds it to a FacetSubmitData object.
        concurrent_process() -> Iterable: retrieves cool pool data and concurrently processes emission data for each associated account.
        submit_to_facet(params=FacetSubmitData): submits CO2 emission data for a cool pool to a smart contract on the Polygon network.
    """


    def __init__(self):
        pass

    async def get_coolpool_details(self):
        # totalDeposit value's unit DepositableToken (i.e. NCT, MCO2, USDT, USDC)

        query = """
            query CoolPools {
                deposits {
                    pool {
                        id
                        accounts {
                            address
                            chainID
                            isActive
                            lastCalculatedBlock
                        }
                    }
                }
            }
        """

        async with aiohttp.ClientSession() as session:
            async with session.post(
                settings.GRAPH_URL, json={"query": query}
            ) as response:
                subgraph = await response.json()
                results = GraphResponse(**subgraph["data"])
                for each in results.deposits:
                    yield each.pool

    async def get_emission_data(self, db_request: DBRequest) -> list[DBResult]:
        async with aiohttp.ClientSession() as session:
            async with session.get(
                settings.DB_URL, params=db_request.dict()
            ) as response:
                return [DBResult(**i) for i in await response.json()]

    async def connect_to_websocket(self, message: WsRequest):
        try:
            async with websockets.connect(settings.WS_URL) as websocket:
                msg_str = json.dumps(message.dict())
                await websocket.send(msg_str)
                async for resp in websocket:
                    response = json.loads(resp)
                    if response["status"] == "0":
                        return True
        except websockets.exceptions.ConnectionClosedOK as e:
            print(f"WebSocket connection closed normally with status code 1000: {e}")
            raise ValidationError(e)
        
    async def process_accounts(self, ws_request: WsRequest, account: Account, facet_data: FacetSubmitData) -> bool:
        try:
            await self.connect_to_websocket(message=ws_request)
        except Exception as err:
            raise ValidationError(err)

        db_request = DBRequest(
            chain_id=ws_request.chain_id,
            addresses=[ws_request.addresses[0]],
            block_start=ws_request.block_start,
        )

        result = await self.get_emission_data(db_request)
        facet_data.targetAddressesEmissions.append(int(result[0].co2 * 1000))
        facet_data.targetAddressesTxCounts.append(result[0].tx_count)
        facet_data.updates.append(
            AccountUpdate(
                chainId=result[0].chain_id.value,
                address_=result[0].address,
                knownLastCalculatedBlock=account.lastCalculatedBlock,
                newLastCalculatedBlock=result[0].block_end,
            )
        )

    async def concurrent_process(self) -> Iterable:
        async for pool in self.get_coolpool_details():
            facet_data = FacetSubmitData(coolPoolID=pool.id, targetAddressesEmissions=[], targetAddressesTxCounts=[], updates=[])

            tasks = []
            for each in pool.accounts:
                if each.chainID == ChainId.CELO:
                    continue
                ws_request = WsRequest(
                    chain_id=each.chainID,
                    addresses=[each.address],
                    block_start=each.lastCalculatedBlock + 1,
                )

                task = asyncio.create_task(self.process_accounts(ws_request, each, facet_data))
                tasks.append(task)

            for task in asyncio.as_completed(tasks):
                await task

            yield facet_data

    async def submit_to_facet(self, params=FacetSubmitData):

        w3 = Web3(Web3.HTTPProvider(settings.WEB3_HTTP_PROVIDER_POLYGON))
        w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        
        with open(settings.ABI_PATH, "r") as f:
            abi = json.load(f)

        contract = w3.eth.contract(address=settings.DIAMOND_ADDRESS_POLYGON, abi=abi)
        # functions = [func['name'] for func in contract.abi if func['type'] == 'function']
        print(params)
        args = [params.coolPoolID, [e.dict() for e in params.updates], params.targetAddressesTxCounts, params.targetAddressesEmissions]
        print(args)

        # Define a dictionary to map error codes to error messages
        error_messages = {
            1: "Invalid input",
            2: "Not enough funds",
            3: "Unauthorized",
            # Add more error codes and messages as needed
        }

        result = None

        try:
            result = contract.functions.submitEmissionForCoolPool(
                *args
            ).transact()
        except ValueError:
            print("Invalid argument")
        except TypeError:
            print("Type error")
        except ContractLogicError as e:
            if isinstance(e.args[0], dict):
                error_code = e.args[0]["code"]
                if error_code in error_messages:
                    print(f"Contract error: {error_messages[error_code]}")
                else:
                    print("Unknown error")
            else:
                print(e.args[0])
        return result

    # async def submit_data_to_api(self, data):
    #     # Submit each object in additional_data to a smart contract function concurrently
    #     contract_results = await asyncio.gather(
    #         *[self.submit_to_smart_contract(obj) for obj in data]
    #     )

    #     return contract_results


async def main():
    oracle = OffsetOracle()
    async for facet_params in oracle.concurrent_process():
        try:
            await oracle.submit_to_facet(params=facet_params)
        except Exception as err:
            print(err)


if __name__ == "__main__":
    asyncio.run(main())
